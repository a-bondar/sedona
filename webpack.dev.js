const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: '/dist',
    hot: true,
    overlay: true,
    historyApiFallback: true,
    proxy: {
      '/api/**': {
        target: 'http://localhost:5000',
        secure: false
      },
      '/auth/google': {
        target: 'http://localhost:5000',
        secure: false
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,

        exclude: /node_modules/,
        use: ['eslint-loader']
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              sourceMap: true,
              minimize: false
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: './postcss.config.js'
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new StyleLintPlugin({
      configFile: 'stylelint.config.js',
      context: 'src',
      files: '**/*.css',
      failOnError: false,
      quiet: false
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
});
