## Учебное full-stack приложение Sedona
  - Авторизация через Google OAUTH 2.0
  - Поиск и бронирование отелей
  - Управление бронированием через ЛК

### Frontend
  - React
  - Redux
  - CSS modules

### Backend
  - Node JS (Express)
  - Passport JS
  - Mongo DB

### Testing
  - Jest
  - Puppeteer (e2e)

### Tools
  - Webpack
  - Eslint
  - Stylelint

### Команды
  - `yarn client` - dev сервер webpack
  - `yarn server` - nodejs сервер
  - `yarn start` - одновременный запуск фронтенда и бекенда
  - `yarn test` - тестирование
  - `yarn build` - сборка
