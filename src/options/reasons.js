const reasons = [
  {
    bg: 'reason-photo-1',
    title: 'Настоящий городок',
    num: 1,
    text: 'Седона не аттракцион для туристов, там течет своя жизнь',
    theme: 'light'
  },
  {
    bg: 'reason-photo-2',
    title: 'Там есть мост дьявола!',
    num: 2,
    text: 'Да, по нему можно пройти! Если конечно вы осмелитесь',
    side: 'right',
    theme: 'light'
  }
];

export default reasons;
