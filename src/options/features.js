const features = [
  {
    _id: 1,
    icon: 'home',
    title: 'Жилье',
    text: 'Рекомендуем пожить в настоящем мотеле, все как в кино!'
  },
  {
    _id: 2,
    icon: 'food',
    title: 'Еда',
    text: 'Всегда заказывай фирменный бургер, вы не разочаруетесь!'
  },
  {
    _id: 3,
    icon: 'souvenirs',
    title: 'Сувениры',
    text: 'Не только китайского, но и местного производства!'
  }
];

export default features;
