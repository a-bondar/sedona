const hotels = [
  {
    id: 1,
    img: 'https://s-ec.bstatic.com/images/hotel/max1024x768/135/135076607.jpg',
    title: 'Cordial Muelle Viejo',
    type: 'Вилла',
    price: 20000,
    stars: 5
  },
  {
    id: 2,
    img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/782/78262736.jpg',
    title: 'Villa Bianca',
    type: 'Вилла',
    price: 150000,
    stars: 4
  },
  {
    id: 3,
    img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/870/87033569.jpg',
    title: 'The Green Mile Villa',
    type: 'Вилла',
    price: 13000,
    stars: 4
  },
  {
    id: 4,
    img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/123/123197762.jpg',
    title: 'XO Hotels Couture',
    type: 'Отель',
    price: 7000,
    stars: 5
  },
  {
    id: 5,
    img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/870/87033569.jpg',
    title: 'The Green Hostel',
    type: 'Хостел',
    price: 3000,
    stars: 2
  },
  {
    id: 6,
    img: 'https://t-ec.bstatic.com/images/hotel/max1024x768/782/78262736.jpg',
    title: 'Bianca Hotel',
    type: 'Отель',
    price: 8200,
    stars: 3
  }
];

export default hotels;
