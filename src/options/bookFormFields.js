import * as validations from '../utils/validations';

const items = [
  {
    label: 'Дата заезда',
    id: 'in',
    name: 'dateIn',
    type: 'date',
    validation: validations.isDateCorrect()
  },
  {
    label: 'Дата выезда',
    id: 'out',
    name: 'dateOut',
    type: 'date',
    validation: validations.isDateBefore()
  },
  {
    label: 'Взрослые',
    id: 'adults',
    name: 'adults',
    type: 'counter',
    validation: validations.required('Выберите хотя бы одного взрослого')
  },
  {
    label: 'Дети',
    id: 'childrens',
    name: 'childrens',
    type: 'counter'
  }
];

export default items;
