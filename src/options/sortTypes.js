const sortTypes = [
  {
    name: 'price',
    value: 'По цене'
  },
  {
    name: 'type',
    value: 'По типу'
  },
  {
    name: 'stars',
    value: 'По рейтингу'
  }
];

export default sortTypes;
