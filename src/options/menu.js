const menu = [
  {
    id: 1,
    url: '/',
    text: 'Информация'
  },
  {
    id: 2,
    text: 'Фото и Видео'
  },
  {
    id: 3,
    url: '/',
    isLogo: true,
    text: 'City of Sedona'
  },
  {
    id: 4,
    text: 'Карта штата'
  },
  {
    id: 5,
    url: '/hotels',
    text: 'Гостиницы'
  }
];

export default menu;
