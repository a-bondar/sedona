import React from 'react';
import * as R from 'ramda';
import Stars from '../components/Stars';

const stars = R.map(
  i => ({ id: String(i), name: String(i), label: <Stars stars={i} /> }),
  R.range(2, 6)
);

export default stars;
