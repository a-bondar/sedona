const hotelTypes = [
  {
    id: 'hotel',
    name: 'hotel',
    label: 'Отель'
  },
  {
    id: 'hostel',
    name: 'hostel',
    label: 'Хостел'
  },
  {
    id: 'villa',
    name: 'villa',
    label: 'Вилла'
  },
  {
    id: 'appartments',
    name: 'appartments',
    label: 'Апартаменты'
  }
];

export default hotelTypes;
