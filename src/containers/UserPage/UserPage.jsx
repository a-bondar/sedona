import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as R from 'ramda';
import { connect } from 'react-redux';

import UserPage from '../../components/UserPage';
import { fetchHotels, cancelBookHotel } from '../../redux/modules/hotels';

const mapStateToProps = (state) => {
  const userId = R.path(['auth', 'user', 'googleId'], state);
  const allHotels = R.path(['hotels', 'data'], state);
  // TODO - добавить мемоизацию
  const hotels = R.filter(item => R.path(['book', 'userId'], item) === userId, allHotels);

  return {
    isUserLoaded: R.path(['auth', 'status', 'success'], state),
    name: R.path(['auth', 'user', 'name'], state),
    email: R.path(['auth', 'user', 'email'], state),
    photo: R.path(['auth', 'user', 'photo'], state),
    hotels
  };
};

class UserPageContainer extends Component {
  static propTypes = {
    isUserLoaded: PropTypes.bool,
    name: PropTypes.string,
    email: PropTypes.string,
    photo: PropTypes.string,
    hotels: PropTypes.array, // eslint-disable-line
    history: PropTypes.object, // eslint-disable-line
    fetchHotels: PropTypes.func,
    cancelBookHotel: PropTypes.func
  };

  static defaultProps = {
    isUserLoaded: false,
    hotels: [],
    name: '',
    email: '',
    photo: '',
    fetchHotels: () => {},
    cancelBookHotel: () => {}
  };

  componentDidMount() {
    if (!this.props.isUserLoaded) {
      this.props.history.replace('/');
    } else {
      this.props.fetchHotels({ all: true });
    }
  }

  additionalHotelProps = {
    isBookInfoVisible: true,
    onCancelOrderClick: this.props.cancelBookHotel
  };

  render() {
    const { isUserLoaded, photo, ...restProps } = this.props;
    const largerPhoto = photo.replace('=50', '=400');

    return (
      isUserLoaded && (
        <UserPage
          {...restProps}
          photo={largerPhoto}
          additionalHotelProps={this.additionalHotelProps}
        />
      )
    );
  }
}

export default R.compose(
  withRouter,
  connect(mapStateToProps, { fetchHotels, cancelBookHotel })
)(UserPageContainer);
