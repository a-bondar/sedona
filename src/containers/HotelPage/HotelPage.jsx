import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as R from 'ramda';

import { fetchHotels } from '../../redux/modules/hotels';
import HotelPage from '../../components/HotelPage';

const mapStateToProps = (state, ownProps) => {
  const hotels = R.pathOr([], ['hotels', 'data'], state);
  const hotelId = R.path(['match', 'params', 'id'], ownProps);
  // TODO - добавить мемоизацию
  const hotel = R.compose(R.head, R.filter(item => (item._id === hotelId)))(hotels); // eslint-disable-line
  const isLoading = R.pathOr(false, ['hotels', 'status', 'loading'], state);
  const isAvailable = R.isEmpty(R.propOr({}, 'book', hotel));
  const isUserLogged = !R.isEmpty(R.pathOr({}, ['auth', 'user'], state));

  return {
    hotel,
    isLoading,
    hotelId,
    isAvailable,
    isUserLogged
  };
};

class HotelPageContainer extends Component {
  static propTypes = {
    fetchHotels: PropTypes.func,
    isLoading: PropTypes.bool,
    isAvailable: PropTypes.bool,
    isUserLogged: PropTypes.bool,
    hotelId: PropTypes.string,
    ...HotelPage.propTypes
  };

  static defaultProps = {
    fetchHotels: () => {},
    isLoading: false,
    isAvailable: false,
    isUserLogged: false,
    hotelId: ''
  };

  componentDidMount() {
    this.props.fetchHotels({ all: true });
  }

  render() {
    const isBookInfoAvailable = this.props.isUserLogged;

    return (
      <HotelPage
        {...this.props.hotel}
        hotelId={this.props.hotelId}
        handleButtonClick={this.toggleForm}
        isBookInfoAvailable={isBookInfoAvailable}
        isFormVisible={this.props.isAvailable}
        isLoading={this.props.isLoading}
      />
    );
  }
}

const enhanced = R.compose(connect(mapStateToProps, { fetchHotels }))(HotelPageContainer);

export default enhanced;
