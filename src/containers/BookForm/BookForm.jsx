/* eslint jsx-a11y/label-has-for: 0 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { connect } from 'react-redux';

import { changeValue } from '../../redux/modules/bookForm';
import { bookHotel } from '../../redux/modules/hotels';
import { getFormErrors } from '../../utils';
import DateInput from '../DateInput';
import Counter from '../Counter';
import Button from '../../components/Button';

import styles from './BookForm.css';

const mapStateToProps = (state, { fields }) => {
  const formValues = R.propOr({}, 'bookForm', state);
  const errors = getFormErrors({ formValues, fields });
  const isValid = R.isEmpty(errors);

  return { formValues, errors, isValid };
};

class BookForm extends PureComponent {
  static propTypes = {
    fields: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string,
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.string,
      defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })),
    changeValue: PropTypes.func,
    handleSubmit: PropTypes.func,
    hotelId: PropTypes.string,
    isValid: PropTypes.bool,
    formValues: PropTypes.object, // eslint-disable-line
    errors: PropTypes.object // eslint-disable-line
  };

  static defaultProps = {
    fields: [],
    changeValue: () => {},
    handleSubmit: () => {},
    formValues: {},
    errors: {},
    isValid: false,
    hotelId: ''
  };

  getField = (type) => {
    switch (type) {
      case 'date':
        return DateInput;
      case 'counter':
        return Counter;
      default:
        return 'input';
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.props.handleSubmit) {
      this.props.handleSubmit({ ...this.props.formValues, hotelId: this.props.hotelId });
    }
  };

  isBtnDisabled = () => {
    const { fields, formValues, isValid } = this.props;

    // TODO - вынести
    return R.length(R.filter(i => i !== '', R.values(formValues))) < R.length(fields) || !isValid;
  };

  renderItem = ({
    label, id, name, type, defaultValue
  }) => {
    const { errors, formValues } = this.props;
    const Field = this.getField(type);
    const error = errors[name];
    const value = formValues[name];

    return (
      <div className={styles.fieldset} key={id}>
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
        <div className={styles.input}>
          <Field
            name={name}
            id={id}
            defaultValue={defaultValue}
            value={value}
            onChange={this.props.changeValue}
            error={error}
          />
        </div>
      </div>
    );
  };

  render() {
    return (
      <form action="#" className={styles.form} onSubmit={this.handleSubmit}>
        <div className={styles.body}>{R.map(this.renderItem, this.props.fields)}</div>
        <div className={styles.btn}>
          <Button
            text="Забронировать"
            type="submit"
            full
            color="blue"
            size="medium"
            disabled={this.isBtnDisabled()}
            data-testid="book"
          />
        </div>
      </form>
    );
  }
}

export default connect(mapStateToProps, { changeValue, handleSubmit: bookHotel })(BookForm);
