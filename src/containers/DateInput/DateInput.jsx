import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Input from '../../components/Input';

const MASK = [/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/];

class DateInput extends PureComponent {
  static propTypes = {
    ...Input.propTypes,
    error: PropTypes.string,
    defaultValue: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string
  };

  static defaultProps = {
    ...Input.defaultProps,
    defaultValue: '',
    error: '',
    name: '',
    value: null
  };

  state = {
    value: this.props.defaultValue
  };

  handleChange = (e) => {
    const value = R.pathOr('', ['target', 'value'], e);

    this.setState({ value });

    if (this.props.onChange) {
      this.props.onChange({ value, name: this.props.name });
    }
  };

  render() {
    const value = R.isNil(this.props.value) ? this.state.value : this.props.value;

    return (
      <Input
        onChange={this.handleChange}
        value={value}
        placeholder="ДД.ММ.ГГГГ"
        mask={MASK}
        error={this.props.error}
        name={this.props.name}
      />
    );
  }
}

export default DateInput;
