import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as R from 'ramda';

import withLoader from '../../decorators/withLoader';
import User from '../../components/User';

const mapStateToProps = state => ({
  user: R.path(['auth', 'user'], state),
  isLoading: R.path(['auth', 'status', 'loading'], state)
});

const UserContainer = ({ user }) => (
  <User
    loginUrl="/auth/google"
    user={user}
    logoutUrl="/api/logout"
    userUrl={`/user/${user.googleId}`}
  />
);

UserContainer.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    photo: PropTypes.string
  })
};

UserContainer.defaultProps = {
  user: {}
};

export default R.compose(connect(mapStateToProps), withLoader)(UserContainer);
