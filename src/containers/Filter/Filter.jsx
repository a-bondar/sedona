import { connect } from 'react-redux';
import * as R from 'ramda';

import { changeOrder, changeType } from '../../redux/modules/filter';
import Filter from '../../components/Filter';

const mapStateToProps = (state) => {
  const type = R.path(['filter', 'type'], state);
  const order = R.path(['filter', 'order'], state);
  const found = R.length(R.pathOr({}, ['hotels', 'data'], state));

  return { type, order, found };
};

const mapDispatchToProps = { onOrderClick: changeOrder, onSortClick: changeType };

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
