import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchHotels } from '../../redux/modules/hotels';

import SearchForm from '../SearchForm/';
import Filter from '../Filter/';
import Hotels from '../Hotels/';

import {
  getSearchSum,
  getSearchHotelsTypes,
  getSearchStarsTypes
} from '../../redux/modules/searchForm';

const mapStateToProps = state => ({
  sum: getSearchSum(state),
  types: getSearchHotelsTypes(state),
  stars: getSearchStarsTypes(state)
});

class HotelsPage extends Component {
  static propTypes = {
    fetchHotels: PropTypes.func,
    stars: PropTypes.arrayOf(PropTypes.string),
    types: PropTypes.arrayOf(PropTypes.string),
    sum: PropTypes.string
  };

  static defaultProps = {
    fetchHotels: () => {},
    stars: [],
    types: [],
    sum: ''
  };

  componentDidMount() {
    const { stars, types, sum } = this.props;

    this.props.fetchHotels({ stars, types, sum });
  }

  render() {
    return (
      <Fragment>
        <SearchForm />
        <Filter />
        <Hotels />
      </Fragment>
    );
  }
}

export default connect(mapStateToProps, { fetchHotels })(HotelsPage);
