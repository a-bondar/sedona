import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchUser } from '../../redux/modules/auth';

import App from '../../components/App';

class AppContainer extends Component {
  static propTypes = {
    fetchUser: PropTypes.func
  };

  static defaultProps = {
    fetchUser: () => {}
  };

  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    return <App />;
  }
}

export default connect(null, { fetchUser })(AppContainer);
