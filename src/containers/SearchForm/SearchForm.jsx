import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as R from 'ramda';

import { getCheckboxOptions } from '../../utils';
import {
  changeInput,
  toggleFilter,
  getSearchSum,
  getSearchHotelsTypes,
  getSearchStarsTypes
} from '../../redux/modules/searchForm';
import { fetchHotels } from '../../redux/modules/hotels';
import * as options from '../../options';
import SearchForm from '../../components/SearchForm';

const mapStateToProps = (state) => {
  const sum = getSearchSum(state);
  const typesFilter = getSearchHotelsTypes(state);
  const starsFilter = getSearchStarsTypes(state);
  const isInvalid = !sum || R.isEmpty(typesFilter) || R.isEmpty(starsFilter);
  // TODO - мемоизировать вычисления
  const hotelsOptions = getCheckboxOptions(options.hotelsTypes, typesFilter);
  const starsOptions = getCheckboxOptions(options.stars, starsFilter);

  return {
    stars: starsFilter,
    types: typesFilter,
    sum,
    isInvalid,
    hotelsOptions,
    starsOptions
  };
};

class SearchFormContainer extends PureComponent {
  static propTypes = {
    toggleFilter: PropTypes.func,
    changeInput: PropTypes.func,
    fetchHotels: PropTypes.func,
    isInvalid: PropTypes.bool,
    stars: PropTypes.arrayOf(PropTypes.string),
    types: PropTypes.arrayOf(PropTypes.string),
    sum: PropTypes.string,
    hotelsOptions: SearchForm.propTypes.hotelsOptions,
    starsOptions: SearchForm.propTypes.starsOptions
  };

  static defaultProps = {
    toggleFilter: () => {},
    changeInput: () => {},
    fetchHotels: () => {},
    isInvalid: true,
    stars: [],
    types: [],
    sum: '',
    hotelsOptions: SearchForm.defaultProps.hotelsOptions,
    starsOptions: SearchForm.defaultProps.starsOptions
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const { stars, types, sum } = this.props;

    this.props.fetchHotels({ stars, types, sum });
  };

  handleFilterChange = (e, type) => {
    const name = R.path(['target', 'name'], e);
    const checked = R.path(['target', 'checked'], e);

    this.props.toggleFilter({ type, data: { name, checked } });
  };

  handleInputChange = (e) => {
    const value = R.path(['target', 'value'], e);
    const name = R.path(['target', 'name'], e);

    this.props.changeInput({ name, value });
  };

  render() {
    return (
      <SearchForm
        onSubmit={this.handleSubmit}
        onInputChange={this.handleInputChange}
        onFilterChange={this.handleFilterChange}
        disabled={this.props.isInvalid}
        hotelsOptions={this.props.hotelsOptions}
        starsOptions={this.props.starsOptions}
        sum={this.props.sum}
      />
    );
  }
}

export default connect(mapStateToProps, {
  changeInput,
  toggleFilter,
  fetchHotels
})(SearchFormContainer);
