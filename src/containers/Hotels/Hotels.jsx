import React from 'react';
import { connect } from 'react-redux';
import * as R from 'ramda';

import List from '../../components/List';
import HotelInfo from '../../components/HotelInfo';
import withLoader from '../../decorators/withLoader';
import { getHotels } from '../../redux/modules/hotels';

const mapStateToProps = (state) => {
  const hotels = getHotels(state);
  const filterType = R.path(['filter', 'type'], state);
  const filterOrder = R.path(['filter', 'order'], state);
  const sort = filterOrder === 'up' ? R.ascend : R.descend;
  const sortBy = sort(R.prop(filterType));
  const isLoading = R.path(['hotels', 'status', 'loading'], state);

  const items = R.sort(sortBy, hotels);

  return { items, isLoading };
};

const Hotels = props => <List {...props} component={HotelInfo} column testid="hotels" />;

export default R.compose(connect(mapStateToProps), withLoader)(Hotels);
