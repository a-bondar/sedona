import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Counter from './Counter';

describe('CounterContainer', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Counter />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the onChange event', () => {
    const mockFn = jest.fn();
    const component = mount(<Counter onChange={mockFn} value={10} />);

    component.find('button').first().simulate('click');

    expect(mockFn).toHaveBeenCalled();
    component.unmount();
  });

  it('should correct handle increment event', () => {
    const initValue = 10;
    const component = mount(<Counter value={initValue} />);

    component.find('button').last().simulate('click');

    expect(component.state().value).toBe(initValue + 1);
    component.unmount();
  });

  it('should correct handle decrement event', () => {
    const initValue = 10;
    const component = mount(<Counter value={initValue} />);

    component.find('button').first().simulate('click');

    expect(component.state().value).toBe(initValue - 1);
    component.unmount();
  });

  it('should prevent decrement event if value === 0', () => {
    const component = mount(<Counter value={0} />);

    component.find('button').first().simulate('click');

    expect(component.state().value).toEqual(0);
    component.unmount();
  });
});
