import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Counter from '../../components/Counter';

class CounterContainer extends PureComponent {
  static propTypes = {
    value: PropTypes.number,
    defaultValue: PropTypes.number,
    name: PropTypes.string,
    error: PropTypes.string,
    onChange: PropTypes.func
  };

  static defaultProps = {
    value: null,
    defaultValue: 0,
    name: '',
    error: '',
    onChange: () => {}
  };

  state = {
    value: this.props.value || this.props.defaultValue
  };

  handleDecClick = (e) => {
    e.preventDefault();

    const { value } = this.state;

    if (value !== 0) {
      const newValue = R.dec(value);

      this.setState({ value: newValue });
      this.props.onChange({ value: newValue, name: this.props.name });
    }
  };

  handleIncClick = (e) => {
    e.preventDefault();

    const newValue = R.inc(this.state.value);

    this.setState({ value: newValue });
    this.props.onChange({ value: newValue, name: this.props.name });
  };

  render() {
    const value = R.isNil(this.props.value) ? this.state.value : this.props.value;

    return (
      <Counter
        value={value}
        onDecClick={this.handleDecClick}
        onIncClick={this.handleIncClick}
        error={this.props.error}
      />
    );
  }
}

export default CounterContainer;
