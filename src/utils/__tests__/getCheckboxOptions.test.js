import { getCheckboxOptions } from '../index';

describe('get checkbox options', () => {
  const mock = {
    input: {
      options: [{ name: 'foo', bar: 'bar' }, { name: 'baz', bar: 'bar' }],
      checkedValues: ['foo']
    },
    output: [{ name: 'foo', bar: 'bar', value: true }, { name: 'baz', bar: 'bar', value: false }]
  };

  it('should work without arguments', () => {
    const result = getCheckboxOptions();

    expect(result).toEqual([]);
  });

  it('should work correct', () => {
    const result = getCheckboxOptions(mock.input.options, mock.input.checkedValues);

    expect(result).toEqual(mock.output);
  });
});
