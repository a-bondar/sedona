import getFormErrors from '../getFormErrors';

describe('get form errors', () => {
  const mocks = {
    input: {
      formValues: {
        foo: 'foo',
        bar: 'bar',
        baz: 'baz'
      },
      fields: [
        {
          name: 'foo',
          validation: val => (val === 'foo' ? 'foo error' : '')
        },
        {
          name: 'bar'
        },
        {
          name: 'baz',
          validation: val => (val === 'baz' ? 'baz error' : '')
        }
      ]
    },
    output: { foo: 'foo error', baz: 'baz error' }
  };

  it('should work without arguments', () => {
    const result = getFormErrors();

    expect(result).toEqual({});
  });

  it('should work correct', () => {
    const result = getFormErrors(mocks.input);

    expect(result).toEqual(mocks.output);
  });
});
