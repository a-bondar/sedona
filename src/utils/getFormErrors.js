import * as R from 'ramda';

const getFormErrors = ({ formValues = {}, fields = [] } = {}) =>
  R.reduce(
    (acc, field) => {
      const validation = R.propOr(() => {}, 'validation', field);
      const error = validation(formValues[field.name], formValues);

      return error ? { ...acc, [field.name]: error } : acc;
    },
    {},
    fields
  );

export default getFormErrors;
