import numbersOnly from '../numbers-only';

describe('numbers only', () => {
  const data = {
    '12po12': '1212',
    QW09: '09',
    '!@!!#02po11': '0211'
  };

  Object.keys(data).forEach(from =>
    it(`should format ${from} to ${data[from]}`, () => {
      expect(numbersOnly(from)).toBe(data[from]);
    }));
});
