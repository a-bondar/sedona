const numbersOnly = (val = '') => val && val.replace(/[^0-9]/g, '');

export default numbersOnly;
