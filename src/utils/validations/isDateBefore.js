import isBefore from 'date-fns/isBefore';
import parse from 'date-fns/parse';
import isDateCorrect from './isDateCorrect';

export const DEFAULT_MSG = 'Дата выезда не может быть ранее даты заезда';

const isDateBefore = ({ msg = DEFAULT_MSG, secondDateKey = 'dateIn' } = {}) => (
  val = '',
  allValues = {}
) => {
  const secondDate = allValues[secondDateKey];
  if (val && secondDate) {
    const parsedFrom = parse(val, 'DD.MM.YYYY', new Date());
    const parsedTo = parse(secondDate, 'DD.MM.YYYY', new Date());
    const isCorrectError = isDateCorrect()(val);

    if (isCorrectError) {
      return isCorrectError;
    }

    return isBefore(parsedFrom, parsedTo) ? msg : '';
  }

  return '';
};

export default isDateBefore;
