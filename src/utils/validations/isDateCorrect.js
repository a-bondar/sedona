import isValid from 'date-fns/isValid';
import parse from 'date-fns/parse';
import isBefore from 'date-fns/isBefore';
import subDays from 'date-fns/subDays';

export const DEFAULT_MSG = 'Введите корректную дату';

const dateValid = (msg = DEFAULT_MSG) => (val = '') => {
  if (val) {
    const now = subDays(new Date(), 1);
    const parsed = parse(val, 'DD.MM.YYYY', new Date());
    const isDateValid = isBefore(now, parsed) && isValid(parse(val, 'DD.MM.YYYY', new Date()));

    return isDateValid ? '' : msg;
  }

  return '';
};

export default dateValid;
