export const DEFAULT_MSG = 'Введите значение';

const required = (msg = DEFAULT_MSG) => val => (val ? '' : msg);

export default required;
