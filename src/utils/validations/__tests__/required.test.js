import required, { DEFAULT_MSG } from '../required';

describe('required', () => {
  const validation = required();

  it('should be NOT valid if value not passed', () => {
    const result = validation();

    expect(result).toBe(DEFAULT_MSG);
  });

  it('should be valid if value passed', () => {
    const result = validation('foo');

    expect(result).toBeFalsy();
  });
});
