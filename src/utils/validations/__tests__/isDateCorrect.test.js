import isDateCorrect, { DEFAULT_MSG } from '../isDateCorrect';

describe('is date correct', () => {
  const validation = isDateCorrect();

  it('should work without arguments', () => {
    const result = validation();

    expect(result).toBeFalsy();
  });

  it('should be NOT valid if date format is incorrect', () => {
    const result = validation('12-021');

    expect(result).toEqual(DEFAULT_MSG);
  });

  it('should be NOT valid if date in past', () => {
    const result = validation('12.01.2010');

    expect(result).toEqual(DEFAULT_MSG);
  });

  it('should be valid if date format is correct an date not in past', () => {
    const result = validation('12.01.2020');

    expect(result).toBeFalsy();
  });
});
