import isDateBefore, { DEFAULT_MSG } from '../isDateBefore';

describe('is date before', () => {
  const validation = isDateBefore();

  it('should work without arguments', () => {
    const result = validation();

    expect(result).toBeFalsy();
  });

  it('should be NOT valid if current date is before second date', () => {
    const result = validation('12.01.2020', { dateIn: '12.01.2021' });

    expect(result).toBe(DEFAULT_MSG);
  });

  it('should be valid if current date is after second date', () => {
    const result = validation('12.01.2020', { dateOut: '12.01.2019' });

    expect(result).toBeFalsy();
  });
});
