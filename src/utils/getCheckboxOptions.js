import * as R from 'ramda';

const getCheckboxOptions = (options = [], checkedValues = []) => {
  if (Array.isArray(options) && !R.isEmpty(options)) {
    return R.map(item => ({ ...item, value: R.contains(item.name, checkedValues) }), options);
  }

  return [];
};

export default getCheckboxOptions;
