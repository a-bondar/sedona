export const pending = () => ({
  loading: false,
  success: false,
  error: false
});

export const start = () => ({
  loading: true,
  success: false,
  failure: false
});

export const success = () => ({
  loading: false,
  success: true,
  error: false
});

export const failure = () => ({
  loading: false,
  success: false,
  error: true
});
