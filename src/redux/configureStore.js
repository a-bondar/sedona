import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import * as reducers from './modules';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //eslint-disable-line
const rootReducer = combineReducers(reducers);
const store = createStore(rootReducer, {}, composeEnhancers(applyMiddleware(reduxThunk)));

export default store;
