import * as R from 'ramda';
import { createSelector } from 'reselect';

export const CHANGE_INPUT = 'searchForm/CHANGE_INPUT';
export const TOGGLE_FILTER = 'searchForm/TOGGLE_FILTER';

export const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_INPUT:
      const name = R.path(['payload', 'name'], action);
      const value = R.path(['payload', 'value'], action);

      return { ...state, [name]: value };

    case TOGGLE_FILTER:
      const type = R.path(['payload', 'type'], action);
      const data = R.pathOr({}, ['payload', 'data'], action);
      const prevFilterTypeState = R.propOr({}, type, state);

      return {
        ...state,
        [type]: { ...prevFilterTypeState, ...{ [data.name]: data.checked } }
      };
    default:
      return state;
  }
};

export const changeInput = ({ name, value }) => ({ type: CHANGE_INPUT, payload: { name, value } });
export const toggleFilter = ({ type, data }) => ({ type: TOGGLE_FILTER, payload: { type, data } });

const getFormValues = state => R.propOr({}, 'searchForm', state);
export const getSearchSum = createSelector([getFormValues], formValues =>
  R.propOr('', 'sum', formValues));

const checkedOnly = R.compose(R.keys, R.filter(val => val));
const getSearchFilter = filterType =>
  createSelector([getFormValues], formValues => checkedOnly(R.propOr({}, filterType, formValues)));

export const getSearchHotelsTypes = getSearchFilter('types');
export const getSearchStarsTypes = getSearchFilter('stars');

export default reducer;
