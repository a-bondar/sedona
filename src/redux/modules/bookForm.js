import * as R from 'ramda';

export const CHANGE_VALUE = 'bookForm/CHANGE_VALUE';

export const initialState = {
  adults: 1,
  childrens: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_VALUE:
      const name = R.path(['payload', 'name'], action);
      const value = R.path(['payload', 'value'], action);

      return { ...state, [name]: value };

    default:
      return state;
  }
};

export const changeValue = ({ name, value }) => ({ type: CHANGE_VALUE, payload: { name, value } });

export default reducer;
