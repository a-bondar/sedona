import * as R from 'ramda';

export const CHANGE_TYPE = 'filter/CHANGE_TYPE';
export const CHANGE_ORDER = 'filter/CHANGE_ORDER';

export const initialState = {
  type: 'price',
  order: 'up'
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_TYPE:
      const type = R.prop('payload', action);
      return { ...state, type };

    case CHANGE_ORDER:
      const order = R.prop('payload', action);
      return { ...state, order };

    default:
      return state;
  }
};

export const changeType = type => ({ type: CHANGE_TYPE, payload: type });
export const changeOrder = order => ({ type: CHANGE_ORDER, payload: order });

export default reducer;
