import axios from 'axios';
import { createSelector } from 'reselect';
import * as R from 'ramda';

import { pending, start, success, failure } from '../helpers/statuses';
import { getHotels } from './hotels';

export const initialState = { user: {}, status: pending() };

export const FETCH_USER_REQUEST = 'auth/FETCH_USER_REQUEST';
export const FETCH_USER_SUCCESS = 'auth/FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'auth/FETCH_USER_FAILURE';

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return { ...state, status: start() };
    case FETCH_USER_SUCCESS:
      return { ...state, user: action.payload, status: success() };
    case FETCH_USER_FAILURE:
      return { ...state, status: failure() };
    default:
      return state;
  }
};

export const fetchUserRequest = () => ({ type: FETCH_USER_REQUEST });
export const fetchUserSuccess = data => ({ type: FETCH_USER_SUCCESS, payload: data });
export const fetchUserFailure = () => ({ type: FETCH_USER_FAILURE });

export const fetchUser = () => async (dispatch) => {
  dispatch(fetchUserRequest());

  try {
    const { data } = await axios.get('/api/current_user');
    dispatch(fetchUserSuccess(data));
  } catch (error) {
    dispatch(fetchUserFailure());
  }
};

const getUserId = state => R.path(['auth', 'user', 'googleId'], state);
export const getUserHotels = createSelector([getHotels, getUserId], (hotels, userId) => {
  console.log(hotels);
  console.log(R.filter(item => R.path(['book', 'userId'], item) === userId, hotels));

  return R.filter(item => R.path(['book', 'userId'], item) === userId, hotels);
});
export default reducer;
