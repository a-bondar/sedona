import axios from 'axios';
import * as R from 'ramda';

import { pending, start, success, failure } from '../helpers/statuses';

require('babel-polyfill');

export const initialState = { data: [], status: pending() };

export const FETCH_HOTELS_REQUEST = 'hotels/FETCH_HOTELS_REQUEST';
export const FETCH_HOTELS_SUCCESS = 'hotels/FETCH_HOTELS_SUCCESS';
export const FETCH_HOTELS_FAILURE = 'hotels/FETCH_HOTELS_FAILURE';

export const BOOK_HOTEL_REQUEST = 'hotels/BOOK_HOTEL_REQUEST';
export const BOOK_HOTEL_SUCCESS = 'hotels/BOOK_HOTEL_SUCCESS';
export const BOOK_HOTEL_FAILURE = 'hotels/BOOK_HOTEL_FAILURE';

export const CANCEL_BOOK_HOTEL_REQUEST = 'hotels/CANCEL_BOOK_HOTEL_REQUEST';
export const CANCEL_BOOK_HOTEL_SUCCESS = 'hotels/CANCEL_BOOK_HOTEL_SUCCESS';
export const CANCEL_BOOK_HOTEL_FAILURE = 'hotels/CANCEL_BOOK_HOTEL_FAILURE';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_HOTELS_REQUEST:
      return { ...state, status: start() };
    case FETCH_HOTELS_SUCCESS:
      return { ...state, data: action.payload, status: success() };
    case FETCH_HOTELS_FAILURE:
      return { ...state, status: failure() };
    case BOOK_HOTEL_REQUEST:
      return { ...state, status: start() };
    case BOOK_HOTEL_SUCCESS:
      // TODO - дописать логику
      return {
        ...state,
        data: R.map((hotel) => {
          if (hotel._id === action.payload._id) { // eslint-disable-line
            return action.payload;
          }
          return hotel;
        }, state.data),
        status: success()
      };
    case BOOK_HOTEL_FAILURE:
      return { ...state, status: failure() };
    case CANCEL_BOOK_HOTEL_REQUEST:
      return { ...state, status: start() };
    case CANCEL_BOOK_HOTEL_SUCCESS:
      return {
        ...state,
        data: R.map((hotel) => {
          if (hotel._id === action.payload._id) { // eslint-disable-line
            return { ...hotel, book: {} };
          }
          return hotel;
        }, state.data),
        status: success()
      };
    case CANCEL_BOOK_HOTEL_FAILURE:
      return { ...state, status: failure() };
    default:
      return state;
  }
};

export const fetchHotelsRequest = () => ({ type: FETCH_HOTELS_REQUEST });
export const fetchHotelsSuccess = data => ({ type: FETCH_HOTELS_SUCCESS, payload: data });
export const fetchHotelsFailure = () => ({ type: FETCH_HOTELS_FAILURE });

export const bookHotelRequest = () => ({ type: BOOK_HOTEL_REQUEST });
export const bookHotelSuccess = data => ({ type: BOOK_HOTEL_SUCCESS, payload: data });
export const bookHotelFailure = () => ({ type: BOOK_HOTEL_FAILURE });

export const cancelBookHotelRequest = () => ({ type: CANCEL_BOOK_HOTEL_REQUEST });
export const cancelBookHotelSuccess = data => ({ type: CANCEL_BOOK_HOTEL_SUCCESS, payload: data });
export const cancelBookHotelFailure = () => ({ type: CANCEL_BOOK_HOTEL_FAILURE });

export const fetchHotels = (formData = {}) => async (dispatch) => {
  dispatch(fetchHotelsRequest());

  try {
    const { data } = await axios.get('/api/hotels', { params: { ...formData } });
    dispatch(fetchHotelsSuccess(data));
  } catch (e) {
    dispatch(fetchHotelsFailure());
  }
};

export const bookHotel = (formData = {}) => async (dispatch) => {
  dispatch(bookHotelRequest());

  try {
    const hotelId = R.prop('hotelId', formData);
    const { data } = await axios.put(`/api/hotels/${hotelId}`, formData);

    dispatch(bookHotelSuccess(data));
  } catch (e) {
    dispatch(bookHotelFailure());
  }
};

export const cancelBookHotel = hotelId => async (dispatch) => {
  dispatch(cancelBookHotelRequest());

  try {
    const { data } = await axios.delete(`/api/hotels/${hotelId}`);

    dispatch(cancelBookHotelSuccess(data));
  } catch (e) {
    dispatch(cancelBookHotelFailure());
  }
};

export const getHotels = state => R.path(['hotels', 'data'], state);

export default reducer;
