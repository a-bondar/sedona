import * as hotelsReducer from '../hotels';
import * as statuses from '../../helpers/statuses';

describe('hotels store', () => {
  describe('actions', () => {
    it('should create an action to fetch hotels request', () => {
      const expectedAction = {
        type: hotelsReducer.FETCH_HOTELS_REQUEST
      };

      expect(hotelsReducer.fetchHotelsRequest()).toEqual(expectedAction);
    });

    it('should create an action to fetch hotels success', () => {
      const payload = [{ foo: 'bar' }];
      const expectedAction = {
        type: hotelsReducer.FETCH_HOTELS_SUCCESS,
        payload
      };

      expect(hotelsReducer.fetchHotelsSuccess(payload)).toEqual(expectedAction);
    });

    it('should create an action to fetch hotels failure', () => {
      const expectedAction = {
        type: hotelsReducer.FETCH_HOTELS_FAILURE
      };

      expect(hotelsReducer.fetchHotelsFailure()).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    it('should return the initial state', () => {
      const result = hotelsReducer.default(undefined, {});

      expect(result).toEqual(hotelsReducer.initialState);
    });

    it(`should handle ${hotelsReducer.FETCH_HOTELS_REQUEST} action type`, () => {
      const action = hotelsReducer.fetchHotelsRequest();
      const result = hotelsReducer.default(undefined, action);

      expect(result).toEqual({
        ...hotelsReducer.initialState,
        status: {
          ...statuses.start()
        }
      });
    });

    it(`should handle ${hotelsReducer.FETCH_HOTELS_SUCCESS} action type`, () => {
      const payload = [{ foo: 'bar' }];
      const action = hotelsReducer.fetchHotelsSuccess(payload);
      const result = hotelsReducer.default(undefined, action);

      expect(result).toEqual({
        ...hotelsReducer.initialState,
        data: payload,
        status: {
          ...statuses.success()
        }
      });
    });

    it(`should handle ${hotelsReducer.FETCH_HOTELS_FAILURE} action type`, () => {
      const action = hotelsReducer.fetchHotelsFailure();
      const result = hotelsReducer.default(undefined, action);

      expect(result).toEqual({
        ...hotelsReducer.initialState,
        status: {
          ...statuses.failure()
        }
      });
    });
  });
});
