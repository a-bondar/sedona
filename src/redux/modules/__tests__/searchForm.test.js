import reducer, {
  CHANGE_INPUT,
  TOGGLE_FILTER,
  toggleFilter,
  changeInput,
  initialState
} from '../searchForm';

describe('searchForm store', () => {
  describe('actions', () => {
    it('should create an action to change input', () => {
      const payload = { name: 'foo', value: 'bar' };
      const expectedAction = {
        type: CHANGE_INPUT,
        payload
      };

      expect(changeInput(payload)).toEqual(expectedAction);
    });

    it('should create an action to toggle filter', () => {
      const payload = { type: 'foo', data: { name: 'bar', checked: true } };
      const expectedAction = {
        type: TOGGLE_FILTER,
        payload
      };

      expect(toggleFilter(payload)).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    it('should return the initial state', () => {
      const result = reducer(undefined, {});

      expect(result).toEqual(initialState);
    });

    it(`should handle "${CHANGE_INPUT}" action type`, () => {
      const payload = { name: 'sum', value: '10' };
      const action = changeInput(payload);
      const result = reducer(initialState, action);

      expect(result).toEqual({ ...initialState, [payload.name]: payload.value });
    });

    it(`should handle "${TOGGLE_FILTER}" action type`, () => {
      const payload = { type: 'types', data: { name: 'hotel', checked: false } };
      const action = toggleFilter(payload);
      const result = reducer(null, action);

      expect(result).toEqual(...initialState, {
        [payload.type]: { [payload.data.name]: payload.data.checked }
      });
    });
  });
});
