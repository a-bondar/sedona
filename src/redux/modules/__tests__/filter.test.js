import reducer, {
  changeType,
  changeOrder,
  CHANGE_ORDER,
  CHANGE_TYPE,
  initialState
} from '../filter';

describe('filter store', () => {
  describe('actions', () => {
    it('should create an action to change type', () => {
      const payload = 'price';
      const expectedAction = {
        type: CHANGE_TYPE,
        payload
      };

      expect(changeType(payload)).toEqual(expectedAction);
    });

    it('should create an action to change order', () => {
      const payload = 'up';
      const expectedAction = {
        type: CHANGE_ORDER,
        payload
      };

      expect(changeOrder(payload)).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    it('should return the initial state', () => {
      const result = reducer(undefined, {});

      expect(result).toEqual(initialState);
    });

    it(`should handle "${CHANGE_TYPE}" action type`, () => {
      const payload = 'price';
      const action = changeType(payload);
      const result = reducer(initialState, action);

      expect(result).toEqual({ ...initialState, type: payload });
    });

    it(`should handle "${CHANGE_ORDER}" action type`, () => {
      const payload = 'up';
      const action = changeOrder(payload);
      const result = reducer(initialState, action);

      expect(result).toEqual({ ...initialState, order: payload });
    });
  });
});
