import reducer, { changeValue, CHANGE_VALUE, initialState } from '../bookForm';

describe('bookForm store', () => {
  describe('actions', () => {
    it('should create an action to change value', () => {
      const payload = { name: 'foo', value: 'bar' };
      const expectedAction = {
        type: CHANGE_VALUE,
        payload
      };

      expect(changeValue(payload)).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    it('should return the initial state', () => {
      const result = reducer(undefined, {});

      expect(result).toEqual(initialState);
    });

    it(`should handle "${CHANGE_VALUE}" action type`, () => {
      const payload = { name: 'foo', value: 'bar' };
      const action = changeValue(payload);
      const result = reducer(initialState, action);

      expect(result).toEqual({ ...initialState, [payload.name]: payload.value });
    });
  });
});
