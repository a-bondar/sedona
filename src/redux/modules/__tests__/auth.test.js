import * as authReducer from '../auth';
import * as statuses from '../../helpers/statuses';

describe('auth store', () => {
  describe('actions', () => {
    it('should create an action to fetch user request', () => {
      const expectedAction = {
        type: authReducer.FETCH_USER_REQUEST
      };

      expect(authReducer.fetchUserRequest()).toEqual(expectedAction);
    });

    it('should create an action to fetch user success', () => {
      const expectedAction = {
        type: authReducer.FETCH_USER_SUCCESS
      };

      expect(authReducer.fetchUserSuccess()).toEqual(expectedAction);
    });

    it('should create an action to fetch user failure', () => {
      const expectedAction = {
        type: authReducer.FETCH_USER_FAILURE
      };

      expect(authReducer.fetchUserFailure()).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    it('should return the initial state', () => {
      const result = authReducer.default(undefined, {});

      expect(result).toEqual(authReducer.initialState);
    });

    it(`should handle ${authReducer.FETCH_USER_REQUEST} action type`, () => {
      const action = authReducer.fetchUserRequest();
      const result = authReducer.default(authReducer.initialState, action);

      expect(result).toEqual({
        ...authReducer.initialState,
        status: statuses.start()
      });
    });

    it(`should handle ${authReducer.FETCH_USER_SUCCESS} action type`, () => {
      const payload = { name: 'hi' };
      const action = authReducer.fetchUserSuccess(payload);
      const result = authReducer.default(authReducer.initialState, action);

      expect(result).toEqual({
        ...authReducer.initialState,
        status: statuses.success(),
        user: payload
      });
    });

    it(`should handle ${authReducer.FETCH_USER_FAILURE} action type`, () => {
      const action = authReducer.fetchUserFailure();
      const result = authReducer.default(authReducer.initialState, action);

      expect(result).toEqual({
        ...authReducer.initialState,
        status: statuses.failure()
      });
    });
  });
});
