import React from 'react';
import PropTypes from 'prop-types';

import styles from './withLoader.css';

const withLoader = (WrappedComponent) => {
  const WithLoader = ({ isLoading, ...restProps }) => (
    <div className={styles.wrapper}>
      {isLoading ? <div className={styles.loader} /> : <WrappedComponent {...restProps} />}
    </div>
  );

  WithLoader.propTypes = {
    isLoading: PropTypes.bool
  };

  WithLoader.defaultProps = {
    isLoading: false
  };

  WithLoader.displayName = `WithLoader(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    'Component'})`;

  return WithLoader;
};

export default withLoader;
