import React from 'react';
import PropTypes from 'prop-types';

import Header from '../Header';
import Footer from '../Footer';

import styles from './Page.css';

const Page = ({ children }) => (
  <div className={styles.page}>
    <div className={styles.header}>
      <Header />
    </div>
    <div className={styles.content}>{children}</div>
    <div className={styles.footer}>
      <Footer />
    </div>
  </div>
);

Page.propTypes = {
  children: PropTypes.node.isRequired
};


export default Page;
