/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Link } from 'react-router-dom';

import Button from '../Button';
import Paragraph from '../Paragraph';
import styles from './User.css';

const User = ({
  user, userUrl, loginUrl, logoutUrl, onLoginClick, onLogoutClick
}) => {
  const isUserExist = !R.isEmpty(user);

  return (
    <div className={styles.user}>
      {isUserExist ? (
        <div className={styles.wrapper}>
          <img src={user.photo} alt={user.name} width="46" height="46" className={styles.img} />
          <div className={styles.info}>
            <div className={styles.name}>
              <Paragraph>
                <Link to={userUrl} className={styles.link} data-testid="goToUserpage">
                  {user.name}
                </Link>
              </Paragraph>
            </div>
            <Button text="Выйти" size="small" onClick={onLogoutClick} href={logoutUrl} data-testid="logout" />
          </div>
        </div>
      ) : (
        <div className={styles.button}>
          <Button
            text="Войти с Google"
            href={loginUrl}
            color="red"
            size="small"
            onClick={onLoginClick}
            data-testid="login"
          />
        </div>
      )}
    </div>
  );
};

User.propTypes = {
  user: PropTypes.shape({
    name: PropTypes.string,
    photo: PropTypes.string
  }),
  onLoginClick: PropTypes.func,
  onLogoutClick: PropTypes.func,
  loginUrl: PropTypes.string,
  logoutUrl: PropTypes.string,
  userUrl: PropTypes.string
};

User.defaultProps = {
  user: {},
  onLoginClick: () => {},
  onLogoutClick: () => {},
  loginUrl: '#',
  logoutUrl: '#',
  userUrl: '#'
};

export default User;
