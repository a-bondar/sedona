import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';

import star from '../../resources/star.svg';
import styles from './Stars.css';

const Stars = ({ stars }) => (
  <div className={styles.stars}>
    {R.compose(R.map(i => <img className={styles.star} src={star} alt="star" width="20px" key={i} />), R.range(0))(stars)}
  </div>
);

Stars.propTypes = {
  stars: PropTypes.number
};

Stars.defaultProps = {
  stars: 1
};

export default Stars;
