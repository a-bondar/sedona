import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Order from './Order';

describe('Order', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Order />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the click event', () => {
    const mockFn = jest.fn();
    const output = mount(<Order onClick={mockFn} />);

    output.find('button').last().simulate('click');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });

  it('should handle state changes', () => {
    const output = mount(<Order />);

    output.find('button').first().simulate('click');
    expect(output.state().selected).toEqual('up');

    output.find('button').last().simulate('click');
    expect(output.state().selected).toEqual('down');

    output.unmount();
  });
});
