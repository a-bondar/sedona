import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import classNames from 'classnames/bind';

import styles from './Order.css';

const cx = classNames.bind(styles);

const arrows = [
  {
    name: 'up',
    value: '&#9650;'
  },
  {
    name: 'down',
    value: '&#9660;'
  }
];

class Order extends Component {
  static propTypes = {
    value: PropTypes.string,
    onClick: PropTypes.func
  };

  static defaultProps = {
    value: '',
    onClick: () => {}
  };

  state = {
    selected: this.props.value || 'up'
  };

  handleClick = (e) => {
    e.preventDefault();

    const name = R.path(['target', 'name'], e);

    if (name !== this.state.selected) {
      this.setState({ selected: name });

      if (this.props.onClick) {
        this.props.onClick(name);
      }
    }
  };

  renderArrow = ({ name, value }) => (
    <button
      key={name}
      name={name}
      className={cx('arrow', { selected: this.state.selected === name })}
      onClick={this.handleClick}
      dangerouslySetInnerHTML={{ __html: value }}
    />
  );

  render() {
    return <div className={styles.order}>{R.map(this.renderArrow, arrows)}</div>;
  }
}

export default Order;
