import React from 'react';
import Heading from '../Heading';
import Paragraph from '../Paragraph';

import styles from './Discover.css';

const Discover = () => (
  <section className={styles.discover}>
    <div className={styles.heading}>
      <Heading>Седона - небольшой городок в Аризоне, заслуживающий большего!</Heading>
    </div>
    <div className={styles.paragraph}>
      <Paragraph>Рассмотрим 5 причин, по которым Седона круче чем Гранд Каньон!</Paragraph>
    </div>
  </section>
);

export default Discover;
