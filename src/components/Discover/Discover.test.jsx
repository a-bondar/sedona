import React from 'react';
import renderer from 'react-test-renderer';
import Discover from './Discover';

describe('Discover', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Discover />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
