import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Input from './Input';

describe('Input', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Input />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the change event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Input onChange={mockFn} />);

    output.find('input').simulate('change');
    expect(mockFn).toHaveBeenCalled();
  });

  it('should handle the focus event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Input onFocus={mockFn} />);

    output.find('input').simulate('focus');
    expect(mockFn).toHaveBeenCalled();
  });

  it('should handle the blur event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Input onBlur={mockFn} />);

    output.find('input').simulate('blur');
    expect(mockFn).toHaveBeenCalled();
  });

  it('should render error when error prop passed', () => {
    const output = shallow(<Input error="error" />);

    expect(output.find('.error')).toHaveLength(1);
  });
});
