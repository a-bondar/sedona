import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import * as R from 'ramda';

import styles from './Input.css';

class Input extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    error: PropTypes.string,
    mask: PropTypes.array, // eslint-disable-line
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    formatter: PropTypes.func, //eslint-disable-line
    value: PropTypes.string
  };

  static defaultProps = {
    type: 'text',
    name: 'input',
    error: '',
    placeholder: '',
    onChange: () => {},
    onFocus: () => {},
    onBlur: () => {},
    value: ''
  };

  handleChange = (e) => {
    const value = R.path(['target', 'value'], e);
    const { onChange, formatter } = this.props;

    if (formatter) {
      const newValue = formatter(value);
      e.target.value = newValue;
    }

    onChange(e);
  };

  render() {
    const Component = this.props.mask ? MaskedInput : 'input';

    return (
      <div className={styles.wrapper}>
        <Component
          className={styles.input}
          mask={this.props.mask}
          onChange={this.handleChange}
          onBlur={this.props.onBlur}
          onFocus={this.props.onFocus}
          placeholder={this.props.placeholder}
          type={this.props.type}
          name={this.props.name}
          value={this.props.value}
          data-testid={this.props.name}
        />
        {this.props.error && <span className={styles.error}>{this.props.error}</span>}
      </div>
    );
  }
}

export default Input;
