import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Button from './Button';

describe('Button', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Button />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the click event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Button onClick={mockFn} />);

    output.simulate('click');
    expect(mockFn).toHaveBeenCalled();
  });
});
