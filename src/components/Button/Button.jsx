import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { Link } from 'react-router-dom';

import styles from './Button.css';

const cx = classNames.bind(styles);

const getComponentType = (props) => {
  if (props.href) {
    return 'a';
  }

  if (props.to) {
    return Link;
  }

  return 'button';
};

const Button = ({
  type, text, href, onClick, full, color, size, disabled, to, ...rest
}) => {
  const Component = getComponentType({ href, to });

  return (
    <Component
      className={cx('button', { full, [`${color}`]: color, [`${size}`]: size })}
      type={type}
      onClick={onClick}
      disabled={disabled}
      href={href}
      to={to}
      {...rest}
    >
      {text}
    </Component>
  );
};
Button.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string,
  color: PropTypes.oneOf(['blue', 'brown', 'red']),
  size: PropTypes.oneOf(['small', 'medium']),
  href: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  full: PropTypes.bool,
  to: PropTypes.string
};

Button.defaultProps = {
  type: 'button',
  text: '',
  color: 'blue',
  size: '',
  href: null,
  onClick: () => {},
  disabled: false,
  full: false,
  to: ''
};

export default Button;
