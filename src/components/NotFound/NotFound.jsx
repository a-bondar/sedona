import React from 'react';
import styles from './NotFound.css';

const NotFound = () => <div className={styles.page}>404 - Ничего не найдено :(</div>;

export default NotFound;
