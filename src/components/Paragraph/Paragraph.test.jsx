import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Paragraph from './Paragraph';

describe('Paragraph', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Paragraph >paragraph</Paragraph>);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should apply theme based on prop', () => {
    const component = shallow(<Paragraph theme="light">Paragraph</Paragraph>);

    expect(component.find('.paragraph_light')).toHaveLength(1);
  });
});
