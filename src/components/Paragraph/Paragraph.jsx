import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

import styles from './Paragraph.css';

const cx = classNames.bind(styles);

const Paragraph = ({ children, theme }) => (
  <p className={cx('paragraph', { [`paragraph_${theme}`]: theme })}>{children}</p>
);

Paragraph.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  theme: PropTypes.string
};

Paragraph.defaultProps = {
  theme: ''
};

export default Paragraph;
