import React, { Fragment } from 'react';

import Promo from '../Promo';
import Discover from '../Discover';
import List from '../List';
import Feature from '../Feature';
import Advantage from '../Advantage';
import Reason from '../Reason';
import { features, advantages, reasons } from '../../options';

const Main = () => (
  <Fragment>
    <Promo />
    <Discover />
    <Reason {...reasons[0]} />
    <List items={features} component={Feature} />
    <Reason {...reasons[1]} />
    <List items={advantages} component={Advantage} />
  </Fragment>
);

export default Main;
