import React from 'react';
import renderer from 'react-test-renderer';
import Feature from './Feature';

describe('Feature', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Feature />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
