import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Heading from '../Heading';
import Paragraph from '../Paragraph';

import styles from './Feature.css';

const Feature = ({ icon, title, text }) => (
  <div className={cx(styles.feature, { [`${styles[icon]}`]: icon })}>
    <div className={styles.title}>
      <Heading>{title}</Heading>
    </div>
    <div className={styles.text}>
      <Paragraph>{text}</Paragraph>
    </div>
  </div>
);

Feature.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string
};

Feature.defaultProps = {
  icon: '',
  title: '',
  text: ''
};

export default Feature;
