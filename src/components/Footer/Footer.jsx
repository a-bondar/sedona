import React from 'react';

import styles from './Footer.css';

const Footer = () => (
  <footer className={styles.footer}>
    <span className={styles.hashtag}>#visitsedona</span>
    <div className={styles.copyright}>
            Website by <span className={styles.studio} />
    </div>
  </footer>
);

export default Footer;
