import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { hotelsTypes } from '../../options';
import SearchForm from './SearchForm';

describe('SearchForm', () => {
  it('should render correctly', () => {
    const component = renderer.create(<SearchForm />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the onSubmit event', () => {
    const mockFn = jest.fn();
    const output = mount(<SearchForm onSubmit={mockFn} />);

    output.find('form').simulate('submit');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });

  it('should handle the onFilterChange event', () => {
    const mockFn = jest.fn();
    const output = mount(<SearchForm onFilterChange={mockFn} hotelsOptions={hotelsTypes} />);

    output
      .find('input[type="checkbox"]')
      .first()
      .simulate('change');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });

  it('should handle the onInputChange event', () => {
    const mockFn = jest.fn();
    const output = mount(<SearchForm onInputChange={mockFn} />);

    output
      .find('input[type="text"]')
      .first()
      .simulate('change');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });
});
