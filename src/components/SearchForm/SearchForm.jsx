import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';

import Checkbox from '../Checkbox';
import Input from '../Input';
import Button from '../Button';
import numbersOnly from '../../utils/formatters';

import styles from './SearchForm.css';

class SearchForm extends PureComponent {
  static propTypes = {
    onFilterChange: PropTypes.func,
    onInputChange: PropTypes.func,
    onSubmit: PropTypes.func,
    disabled: PropTypes.bool,
    hotelsOptions: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      label: PropTypes.string
    })),
    starsOptions: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      label: PropTypes.object
    })),
    sum: PropTypes.string
  };

  static defaultProps = {
    onFilterChange: () => {},
    onInputChange: () => {},
    onSubmit: () => {},
    disabled: false,
    hotelsOptions: [],
    starsOptions: [],
    sum: ''
  };

  handleFilterChange = type => (...args) => this.props.onFilterChange(...args, type);

  renderFilter = type => item => (
    <div className={styles.filter} key={item.id}>
      <Checkbox {...item} onChange={this.handleFilterChange(type)} />
    </div>
  );

  render() {
    return (
      <form action="#" className={styles.searchForm} onSubmit={this.props.onSubmit} data-testid="searchForm">
        <div className={styles.wrapper}>
          <div className={styles.filters}>
            <h4 className={styles.heading}>Тип жилья</h4>
            {R.map(this.renderFilter('types'), this.props.hotelsOptions)}
          </div>
          <div className={styles.filters}>
            <h4 className={styles.heading}>Кол-во звезд</h4>
            {R.map(this.renderFilter('stars'), this.props.starsOptions)}
          </div>
        </div>
        <div className={styles.sum}>
          <h4 className={styles.heading}>Сумма</h4>
          <div className={styles.input}>
            <label htmlFor="sum" className={styles.label}>
              До
            </label>
            <Input
              name="sum"
              id="sum"
              onChange={this.props.onInputChange}
              formatter={numbersOnly}
              value={this.props.sum}
            />
          </div>
          <div className={styles.button}>
            <Button
              text="Показать"
              size="medium"
              full
              type="submit"
              disabled={this.props.disabled}
              data-testid="submit"
            />
          </div>
        </div>
      </form>
    );
  }
}

export default SearchForm;
