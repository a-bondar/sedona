import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Heading from '../Heading';
import Paragraph from '../Paragraph';
import Button from '../Button';
import Stars from '../Stars';

import styles from './HotelInfo.css';

class HotelInfo extends PureComponent {
  handleCancelOrderClick = () => {
    if (this.props.onCancelOrderClick) {
      this.props.onCancelOrderClick(this.props._id); // eslint-disable-line
    }
  }

  render() {
    const {
      img, title, type, price, stars, isBookInfoVisible, book, _id
    } = this.props;
    const href = `${this.props.href}/${_id}`;
    return (
      <article className={styles.hotel}>
        <div className={styles.wrapper}>
          <img src={img} alt={title} className={styles.image} width="135" height="90" />
          <div className={styles.info}>
            <div className={styles.title}>
              <Heading>{title}</Heading>
            </div>
            <div className={styles.descr}>
              <div className={styles.text}>
                <Paragraph>{type}</Paragraph>
              </div>
              <Paragraph>от {price} Р.</Paragraph>
            </div>
            <div className={styles.buttons}>
              {href && (
                <div className={styles.button}>
                  <Button text="Подробнее" color="blue" size="small" to={href} data-testid="more" />
                </div>
              )}
              {isBookInfoVisible && (
                <div className={styles.button}>
                  <Button
                    text="Отменить бронирование"
                    color="red"
                    size="small"
                    onClick={this.handleCancelOrderClick}
                    data-testid="cancelBook"
                  />
                </div>
              )}
            </div>
          </div>

          <div className={styles.stars}>
            <Stars stars={stars} />
          </div>
        </div>
        {isBookInfoVisible && (
          <div className={styles.book}>
            <Paragraph>Дата въезда: {book.dateIn}</Paragraph>
            <Paragraph>Дата выезда: {book.dateOut}</Paragraph>
            <Paragraph>Кол-во взрослых: {book.adults}</Paragraph>
            <Paragraph>Кол-во детей: {book.childrens}</Paragraph>
          </div>
        )}
      </article>
    );
  }
}

HotelInfo.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string,
  type: PropTypes.string,
  price: PropTypes.number,
  stars: PropTypes.number,
  href: PropTypes.string,
  onCancelOrderClick: PropTypes.func,
  isBookInfoVisible: PropTypes.bool,
  _id: PropTypes.string,
  book: PropTypes.object //eslint-disable-line
};

HotelInfo.defaultProps = {
  img: '',
  title: '',
  type: '',
  price: 0,
  stars: 1,
  href: '/hotels',
  onCancelOrderClick: () => {},
  isBookInfoVisible: false,
  _id: '',
  book: {}
};

export default HotelInfo;
