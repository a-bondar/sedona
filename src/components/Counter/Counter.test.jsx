import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Counter from './Counter';

describe('Counter', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Counter />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the decrement click event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Counter onDecClick={mockFn} />);

    output.find('button').first().simulate('click');

    expect(mockFn).toHaveBeenCalled();
  });

  it('should handle the increment click event', () => {
    const mockFn = jest.fn();
    const output = shallow(<Counter onIncClick={mockFn} />);

    output.find('button').last().simulate('click');

    expect(mockFn).toHaveBeenCalled();
  });
});
