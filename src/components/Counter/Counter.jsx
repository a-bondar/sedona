import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './Counter.css';

const Counter = ({
  value, error, onDecClick, onIncClick
}) => (
  <div className={styles.wrapper}>
    <div className={styles.counter}>
      <button className={cx([styles.button, styles.minus])} onClick={onDecClick} />
      <span>{value}</span>
      <button className={cx([styles.button, styles.plus])} onClick={onIncClick} />
    </div>
    {error && <span className={styles.error}>{error}</span>}
  </div>
);

Counter.propTypes = {
  value: PropTypes.number,
  error: PropTypes.string,
  onDecClick: PropTypes.func,
  onIncClick: PropTypes.func
};

Counter.defaultProps = {
  value: 0,
  error: '',
  onDecClick: () => {},
  onIncClick: () => {}
};

export default Counter;
