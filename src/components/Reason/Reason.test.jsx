import React from 'react';
import renderer from 'react-test-renderer';
import Reason from './Reason';

describe('Reason', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Reason />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
