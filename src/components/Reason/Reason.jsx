import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import Advantage from '../Advantage';

import styles from './Reason.css';

const cx = classnames.bind(styles);

const Reason = ({ bg, side, ...restProps }) => {
  const src = require(`../../resources/${bg}.jpg`); // eslint-disable-line

  return (
    <section className={styles.reason}>
      <div className={cx('advantage', { [`advantage_${side}`]: side })}>
        <Advantage {...restProps} />
      </div>
      <img className={styles.img} src={src} alt="reason" />
    </section>
  );
};
Reason.propTypes = {
  bg: PropTypes.string,
  side: PropTypes.string
};

Reason.defaultProps = {
  bg: '',
  side: ''
};

export default Reason;
