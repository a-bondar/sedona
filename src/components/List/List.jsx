import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import * as R from 'ramda';

import styles from './List.css';

const cx = classNames.bind(styles);

class List extends PureComponent {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    column: PropTypes.bool,
    testid: PropTypes.string,
    additional: PropTypes.object // eslint-disable-line
  };

  static defaultProps = {
    items: [],
    column: false,
    additional: {},
    testid: ''
  };

  renderItem = (props) => {
    const Component = this.props.component;
    const id = props._id; // eslint-disable-line

    return (
      <li className={styles.item} key={id}>
        <Component {...props} {...this.props.additional} />
      </li>
    );
  };

  render() {
    const { items, column, testid } = this.props;
    return (
      <ul className={cx('list', { column })} data-testid={testid}>
        {R.map(this.renderItem, items)}
      </ul>
    );
  }
}

export default List;
