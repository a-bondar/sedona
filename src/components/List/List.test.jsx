import React from 'react';
import renderer from 'react-test-renderer';
import Feature from '../Feature';
import List from './List';
import { features } from '../../options';

describe('List', () => {
  it('should render correctly', () => {
    const component = renderer.create(<List component={Feature} items={features} />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
