import React from 'react';
import Menu from '../Menu';
import User from '../../containers/User';
import { menu } from '../../options';

import styles from './Header.css';

const Header = () => (
  <header className={styles.header}>
    <div className={styles.menu}>
      <Menu list={menu} />
    </div>
    <div className={styles.user}>
      <User />
    </div>
  </header>
);

export default Header;
