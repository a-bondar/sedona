import React from 'react';
import renderer from 'react-test-renderer';
import Promo from './Promo';

describe('Promo', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Promo />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
