import React from 'react';
import PropTypes from 'prop-types';

import styles from './Promo.css';

const Promo = ({ text, title, subtitle }) => (
  <section className={styles.promo}>
    <p className={styles.text}>{text}</p>
    <h1 className={styles.title}>{title}</h1>
    <blockquote className={styles.subtitle}>{subtitle}</blockquote>
  </section>
);

Promo.propTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string
};

Promo.defaultProps = {
  text: 'Welcome to the gorgeous',
  title: 'Sedona',
  subtitle: 'Because the Grand Canyon sucks!'
};

export default Promo;
