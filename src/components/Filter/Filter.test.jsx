import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Filter from './Filter';

describe('Filter', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Filter />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the onSort click event', () => {
    const mockFn = jest.fn();
    const output = mount(<Filter onSortClick={mockFn} />);

    output
      .find('.sortType')
      .find('button')
      .first()
      .simulate('click');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });

  it('should handle the onOrder click event', () => {
    const mockFn = jest.fn();
    const output = mount(<Filter onOrderClick={mockFn} />);

    output
      .find('.order')
      .find('button')
      .last()
      .simulate('click');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });
});
