import React from 'react';
import PropTypes from 'prop-types';

import Heading from '../Heading';
import SortType from '../SortType';
import Order from '../Order';
import * as options from '../../options';

import styles from './Filter.css';

const Filter = ({
  found, type, order, onSortClick, onOrderClick, sortTypes
}) => (
  <div className={styles.filter}>
    <Heading>Найдено: {found}</Heading>
    <SortType types={sortTypes} value={type} onClick={onSortClick} />
    <Order value={order} onClick={onOrderClick} />
  </div>
);

Filter.propTypes = {
  found: PropTypes.number,
  type: PropTypes.string,
  order: PropTypes.string,
  onSortClick: PropTypes.func,
  onOrderClick: PropTypes.func,
  sortTypes: SortType.propTypes.types // eslint-disable-line
};

Filter.defaultProps = {
  found: 0,
  type: '',
  order: '',
  onSortClick: () => {},
  onOrderClick: () => {},
  sortTypes: options.sortTypes
};

export default Filter;
