import React from 'react';
import renderer from 'react-test-renderer';
import Advantage from './Advantage';


describe('Advantage', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Advantage />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
