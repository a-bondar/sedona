import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Heading from '../Heading';
import Paragraph from '../Paragraph';

import styles from './Advantage.css';

const cx = classNames.bind(styles);

const Advantage = ({
  title, num, text, theme
}) => (
  <div className={cx('advantage', { [`advantage_${theme}`]: theme })}>
    <div className={styles.heading}>
      <Heading theme={theme}>{title}</Heading>
    </div>
    <div className={styles.numWrapper}>
      <span className={styles.num}>№ {num}</span>
    </div>
    <div className={styles.text}>
      <Paragraph theme={theme}>{text}</Paragraph>
    </div>
  </div>
);

Advantage.propTypes = {
  title: PropTypes.string,
  num: PropTypes.number,
  text: PropTypes.string,
  theme: PropTypes.string
};

Advantage.defaultProps = {
  title: '',
  num: 0,
  text: '',
  theme: ''
};

export default Advantage;
