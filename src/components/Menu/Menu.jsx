/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Link, withRouter } from 'react-router-dom';
import classNames from 'classnames/bind';
import Logo from '../Logo';

import styles from './Menu.css';

const cx = classNames.bind(styles);

class Menu extends PureComponent {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.shape({
      url: PropTypes.string,
      text: PropTypes.string,
      isLogo: PropTypes.bool
    })),
    location: PropTypes.object //eslint-disable-line
  };

  static defaultProps = {
    list: [],
    location: {}
  };

  renderItem = ({
    url, text, isLogo, id
  }) => {
    const currentUrl = R.prop('pathname', this.props.location);
    const isActive = currentUrl === url;
    const MenuItem = url ? Link : 'span';
    const isDisabled = !url;

    return (
      <li className={styles.item} key={id}>
        {isLogo ? (
          <Logo url={url} text={text} />
        ) : (
          <MenuItem className={cx('link', { active: isActive, disabled: isDisabled })} to={url}>
            {text}
          </MenuItem>
        )}
      </li>
    );
  };

  render() {
    return <ul className={styles.menu}>{R.map(this.renderItem, this.props.list)}</ul>;
  }
}

export default withRouter(Menu);
