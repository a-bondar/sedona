import React from 'react';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import Menu from './Menu';

describe('Menu', () => {
  it('should render correctly', () => {
    const component = renderer.create(<BrowserRouter>
      <Menu />
                                      </BrowserRouter>);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
