import React from 'react';
import { hot } from 'react-hot-loader';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Page from '../Page';
import Main from '../Main';
import HotelsPage from '../../containers/HotelsPage';
import HotelPage from '../../containers/HotelPage';
import UserPage from '../../containers/UserPage';
import NotFound from '../NotFound';

const App = () => (
  <BrowserRouter>
    <Page>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/hotels" component={HotelsPage} />
        <Route exact path="/hotels/:id" component={HotelPage} />
        <Route exact path="/user/:id" component={UserPage} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Page>
  </BrowserRouter>
);

export default hot(module)(App);
