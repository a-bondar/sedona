import React from 'react';
import { shallow } from 'enzyme';
import puppeteer from 'puppeteer';
import App from './App';
import config from '../../server/config/keys';

let browser;
let page;
beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: false,
    slowMo: 100,
    devtools: true
  });
  const context = await browser.createIncognitoBrowserContext();
  page = await context.newPage();
  await page.goto('http://localhost:8080/hotels');
  page.setViewport({ width: 1024, height: 2400 });
});

describe('App', () => {
  it('should render correctly', () => {
    const component = shallow(<App />);

    expect(component).toMatchSnapshot();
  });
});

describe('on page load ', () => {
  test(
    'login with Google works correctly',
    async () => {
      const loginBtn = await page.$('[data-testid="login"]');

      await loginBtn.click();
      await page.mainFrame().waitForSelector('#identifierId');
      await page.type('#identifierId', config.google.testUser);
      await page.mainFrame().waitForSelector('#identifierNext');
      await page.click('#identifierNext');
      await page.mainFrame().waitForSelector('#password input[type="password"]', { visible: true });
      await page.type('#password input[type="password"]', config.google.testPassword, {
        delay: 100
      });
      await page.click('#passwordNext', { delay: 100 });
      await page.waitForSelector('[data-testid="logout"]');
      const logout = await page.$('[data-testid="logout"]');

      expect(logout).toBeDefined();
    },
    40000
  );

  test(
    'search form works correctly',
    async () => {
      const SUM_INPUT_SELECTOR = '[data-testid="sum"]';
      const HOTELS_SELECTOR = '[data-testid="hotels"]';

      await page.evaluate(() => {
        const elements = document.querySelectorAll('[data-testid="checkbox"]');
        elements.forEach(el => el.click());
      });
      const searchInput = await page.$(SUM_INPUT_SELECTOR);
      const searchSubmit = await page.$('[data-testid="submit"]');

      await searchInput.click();
      await page.type(SUM_INPUT_SELECTOR, '100000');
      await searchSubmit.click();

      await page.waitForSelector(HOTELS_SELECTOR);

      const hotels = await page.$$(`${HOTELS_SELECTOR} li`);

      expect(hotels).not.toHaveLength(0);
    },
    20000
  );

  test(
    'book hotel form work correctly',
    async () => {
      const DATE_IN_SELECTOR = '[data-testid="dateIn"]';
      const DATE_OUT_SELECTOR = '[data-testid="dateOut"]';
      const BOOK_FORM_SELECTOR = '[data-testid="bookForm"]';

      const moreBtn = await page.$('[data-testid="more"]');

      await moreBtn.click();

      await page.waitForSelector(BOOK_FORM_SELECTOR);

      const dateIn = await page.$(DATE_IN_SELECTOR);
      const dateOut = await page.$(DATE_OUT_SELECTOR);
      const bookBtn = await page.$('[data-testid="book"]');

      await dateIn.click();
      await page.type(DATE_IN_SELECTOR, '20.01.2021');

      await dateOut.click();
      await page.type(DATE_OUT_SELECTOR, '23.01.2021');

      await bookBtn.click();

      const bookForm = await page.$(BOOK_FORM_SELECTOR);

      expect(bookForm).toBeNull();
    },
    16000
  );

  test(
    'remove book hotel work correct',
    async () => {
      const ORDERS_SELECTOR = '[data-testid="orders"]';
      const gotToUserPage = await page.$('[data-testid="goToUserpage"]');

      await gotToUserPage.click();

      page.waitForSelector(ORDERS_SELECTOR);

      const cancelBookBtn = await page.$('[data-testid="cancelBook"]');

      await cancelBookBtn.click();
      await page.waitFor(4000);

      const orders = await page.$(ORDERS_SELECTOR);

      expect(orders).toBeNull();
    },
    20000
  );
});

afterAll(() => browser.close());
