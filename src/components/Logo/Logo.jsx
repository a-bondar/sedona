import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../resources/logo.png';

import styles from './Logo.css';

const Logo = ({ text }) => (
  <div className={styles.logo}>
    <img className={styles.img} src={logo} width="139" height="70" alt={text} title={text} />
  </div>
);

Logo.propTypes = {
  text: PropTypes.string
};

Logo.defaultProps = {
  text: 'Sedona'
};

export default Logo;
