import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Heading from '../Heading';
import Paragraph from '../Paragraph';
import List from '../List';
import HotelInfo from '../HotelInfo';

import styles from './UserPage.css';

const UserPage = ({
  photo, name, email, hotels, additionalHotelProps
}) => (
  <div className={styles.page}>
    <div className={styles.info}>
      <img src={photo} alt={photo} className={styles.image} />
      <div className={styles.wrapper}>
        {name && (
          <div className={styles.name}>
            <Heading>Имя</Heading>
            <Paragraph>{name}</Paragraph>
          </div>
        )}
        {email && (
          <Fragment>
            <Heading>Email</Heading>
            <Paragraph>{email}</Paragraph>
          </Fragment>
        )}
      </div>
    </div>
    {!R.isEmpty(hotels) && (
      <div className={styles.orders} data-testid="orders">
        <Heading>Забронированные отели</Heading>
        <List component={HotelInfo} items={hotels} column additional={additionalHotelProps} />
      </div>
    )}
  </div>
);

UserPage.propTypes = {
  name: PropTypes.string,
  photo: PropTypes.string,
  email: PropTypes.string,
  hotels: PropTypes.array, // eslint-disable-line
  additionalHotelProps: PropTypes.object // eslint-disable-line
};

UserPage.defaultProps = {
  name: '',
  photo: '',
  email: '',
  hotels: [],
  additionalHotelProps: {}
};

export default UserPage;
