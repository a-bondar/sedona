/* eslint jsx-a11y/label-has-for: 0 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import * as R from 'ramda';

import styles from './Checkbox.css';

const cx = classNames.bind(styles);

class Checkbox extends Component {
  static propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    value: PropTypes.bool,
    onChange: PropTypes.func
  };

  static defaultProps = {
    name: '',
    id: '',
    label: '',
    value: null,
    onChange: () => {}
  };

  state = {
    value: this.props.value || false
  };

  handleChange = (e) => {
    const value = R.path(['target', 'checked'], e);

    this.setState({ value });

    if (this.props.onChange) {
      this.props.onChange(e);
    }
  };

  render() {
    const value = R.isNil(this.props.value) ? this.state.value : this.props.value;
    const { id, name, label } = this.props;

    return (
      <div className={cx('checkbox', { checked: value })}>
        <input
          className={styles.input}
          id={id}
          type="checkbox"
          name={name}
          checked={value}
          onChange={this.handleChange}
          data-testid="checkbox"
        />
        <label className={styles.label} htmlFor={id}>
          {label}
        </label>
      </div>
    );
  }
}

export default Checkbox;
