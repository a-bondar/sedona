import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Checkbox from './Checkbox';

describe('Checkbox', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Checkbox />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the onChange event', () => {
    const mockFn = jest.fn();
    const output = mount(<Checkbox onChange={mockFn} />);

    output.find('input').simulate('change');
    expect(mockFn).toHaveBeenCalled();
    output.unmount();
  });
});
