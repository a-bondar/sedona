import React from 'react';

import Form from '../../containers/BookForm';
import { bookFormFields } from '../../options';

import styles from './Search.css';

const Search = props => (
  <section className={styles.search}>
    <div className={styles.form}>
      <Form fields={bookFormFields} {...props} />
    </div>
  </section>
);

export default Search;
