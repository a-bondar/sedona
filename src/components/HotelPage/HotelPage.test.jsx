import React from 'react';
import renderer from 'react-test-renderer';
import HotelPage from './HotelPage';

describe('HotelPage', () => {
  it('should render correctly', () => {
    const component = renderer.create(<HotelPage />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
