import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import withLoader from '../../decorators/withLoader';
import Heading from '../Heading';
import Search from '../Search';
import Paragraph from '../Paragraph';
import Stars from '../Stars';
import styles from './HotelPage.css';

const HotelPage = ({
  title,
  img,
  type,
  description,
  stars,
  isFormVisible,
  isBookInfoAvailable,
  hotelId
}) => (
  <div className={styles.page}>
    <img src={img} alt={title} className={styles.image} width="135" height="90" />
    <div className={styles.title}>
      <Heading>{title}</Heading>
      <div className={styles.stars}>
        <Stars stars={stars} />
      </div>
    </div>
    <div className={styles.subtitle}>
      <Heading>{type}</Heading>
    </div>
    <div className={styles.description}>
      <Paragraph>{description}</Paragraph>
    </div>
    {isBookInfoAvailable && (
      <Fragment>
        <div className={styles.title}>
          <Heading>{isFormVisible ? `Забронировать ${type}` : `${type} забронирован`}</Heading>
        </div>
        {isFormVisible && (
          <div data-testid="bookForm">
            <Search hotelId={hotelId} />
          </div>
        )}
      </Fragment>
    )}
  </div>
);

HotelPage.propTypes = {
  title: PropTypes.string,
  img: PropTypes.string,
  type: PropTypes.string,
  stars: PropTypes.number,
  description: PropTypes.string,
  isFormVisible: PropTypes.bool,
  hotelId: PropTypes.string,
  isBookInfoAvailable: PropTypes.bool
};

HotelPage.defaultProps = {
  title: '',
  img: '',
  type: '',
  stars: 1,
  isFormVisible: false,
  isBookInfoAvailable: false,
  hotelId: '',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
};

export default withLoader(HotelPage);
