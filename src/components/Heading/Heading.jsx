import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

import styles from './Heading.css';

const cx = classNames.bind(styles);

const Heading = ({ children, theme }) => (
  <h2 className={cx('heading', { [`heading_${theme}`]: theme })}>{children}</h2>
);

Heading.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  theme: PropTypes.string
};

Heading.defaultProps = {
  theme: ''
};

export default Heading;
