import React from 'react';
import renderer from 'react-test-renderer';
import Heading from './Heading';

describe('Heading', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Heading >Heading</Heading>);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
