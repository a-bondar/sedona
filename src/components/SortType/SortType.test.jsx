import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { sortTypes } from '../../options';
import SortType from './SortType';

describe('SortType', () => {
  it('should render correctly', () => {
    const component = renderer.create(<SortType />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should handle the click event', () => {
    const mockFn = jest.fn();
    const output = mount(<SortType onClick={mockFn} types={sortTypes} />);

    output
      .find('button')
      .first()
      .simulate('click');
    expect(mockFn).toHaveBeenCalled();

    output.unmount();
  });

  it('should handle state changes', () => {
    const output = mount(<SortType types={sortTypes} />);

    output.find('button').forEach((node, idx) => {
      const item = sortTypes[idx];

      node.simulate('click');
      expect(output.state().selected).toEqual(item.name);
    });

    output.unmount();
  });
});
