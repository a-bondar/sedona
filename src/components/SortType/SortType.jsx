import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import classNames from 'classnames/bind';

import styles from './SortType.css';

const cx = classNames.bind(styles);

class SortType extends Component {
  static propTypes = {
    value: PropTypes.string,
    types: PropTypes.arrayOf(PropTypes.shape({ name: PropTypes.string, value: PropTypes.string })),
    onClick: PropTypes.func
  };

  static defaultProps = {
    value: '',
    types: [],
    onClick: () => {}
  };

  state = {
    selected: this.props.value
  };

  handleClick = (e) => {
    e.preventDefault();

    const name = R.path(['target', 'name'], e);

    if (name !== this.state.selected) {
      this.setState({ selected: name });

      if (this.props.onClick) {
        this.props.onClick(name);
      }
    }
  };

  renderType = ({ name, value }) => (
    <li className={styles.item} key={name}>
      <button
        onClick={this.handleClick}
        className={cx('type', { selected: this.state.selected === name })}
        name={name}
      >
        {value}
      </button>
    </li>
  );

  render() {
    return (
      <div className={styles.sortType}>
        <p className={styles.text}>Сортировка:</p>
        <ul className={styles.list}>{R.map(this.renderType, this.props.types)}</ul>
      </div>
    );
  }
}

export default SortType;
