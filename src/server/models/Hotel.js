const mongoose = require('mongoose');

const hotelSchema = new mongoose.Schema({
  img: String,
  title: String,
  type: String,
  code: String,
  description: String,
  price: Number,
  stars: Number,
  book: {
    userId: String,
    dateIn: String,
    dateOut: String,
    adults: Number,
    childrens: Number
  }
});

mongoose.model('hotels', hotelSchema);
