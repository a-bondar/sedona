require('./models/Hotel');
require('./models/User');
require('./services/passport');

const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');

const keys = require('./config/keys');

const app = express();

mongoose.connect(keys.mongoURI);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieSession({ maxAge: 30 * 24 * 60 * 60 * 1000, keys: [keys.cookieKey] }));
app.use(passport.initialize());
app.use(passport.session());

require('./routes/hotels')(app);
require('./routes/auth')(app);

// const PORT = process.env.PORT || 5000;

// app.listen(PORT, () => {
//   console.log(`server running at http://localhost:${PORT}`);
// });


module.exports = app;
