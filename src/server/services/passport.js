const R = require('ramda');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  // User model instance ID
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id);

  done(null, user);
});

passport.use(new GoogleStrategy(
  {
    clientID: keys.google.clientID,
    clientSecret: keys.google.clientSecret,
    callbackURL: '/auth/google/callback'
  },
  async (accessToken, refreshToken, profile, done) => {
    const googleId = R.prop('id', profile);
    const existingUser = await User.findOne({ googleId });

    if (!existingUser) {
      const name = R.prop('displayName', profile);
      const photo = R.compose(R.prop('value'), R.head, R.propOr([], 'photos'))(profile);
      const email = R.compose(R.prop('value'), R.head, R.propOr([], 'emails'))(profile);

      const newUser = new User({
        googleId, name, photo, email
      }).save();

      done(null, newUser);
    } else {
      done(null, existingUser);
    }
  }
));
