const passport = require('passport');

module.exports = (app) => {
  app.get(
    '/auth/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    })
  );

  app.get(
    '/auth/google/callback',
    passport.authenticate('google', { successRedirect: '/hotels' })
  );

  app.get('/api/current_user', (req, res) => {
    // req.user добавляет миддлвар паспорта
    res.send(req.user);
  });

  app.get('/api/logout', (req, res) => {
    // req.logout добавляет миддлвар паспорта
    req.logout();
    res.redirect('/');
  });
};
