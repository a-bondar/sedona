const R = require('ramda');
const mongoose = require('mongoose');

const Hotel = mongoose.model('hotels');

module.exports = (app) => {
  const PATH = '/api/hotels';

  app.get(PATH, async (req, res) => {
    const query = R.propOr({}, 'query', req);
    let result;

    try {
      if (query.all) {
        result = await Hotel.find();
      } else {
        result = await Hotel.find({ stars: { $in: query.stars }, code: { $in: query.types } })
          .where('price')
          .lt(query.sum);
      }

      res.send(result);
    } catch (error) {
      res.status(404).send({ error: 'An error has occurred' });
    }
  });

  app.get(`${PATH}/:id`, async (req, res) => {
    const id = R.path(['params', 'id'], req);

    try {
      const result = await Hotel.findById(id);

      res.send(result);
    } catch (err) {
      res.status(404).send({ error: 'An error has occurred' });
    }
  });

  app.put(`${PATH}/:id`, async (req, res) => {
    try {
      const { hotelId, ...restData } = R.propOr({}, 'body', req);
      const userId = R.path(['user', 'googleId'], req);
      const hotel = await Hotel.findById(hotelId);

      if (hotel && userId) {
        const data = { book: { ...restData, userId } };

        const updatedHotel = await Hotel.findByIdAndUpdate(hotelId, data, { new: true });

        res.send(updatedHotel);
      }
    } catch (err) {
      res.status(404).send({ error: 'An error has occurred' });
    }
  });

  app.delete(`${PATH}/:id`, async (req, res) => {
    try {
      const hotelId = R.path(['params', 'id'], req);
      const hotel = await Hotel.findById(hotelId);

      if (hotel) {
        const data = { book: {} };
        const updatedHotel = await Hotel.findByIdAndUpdate(hotelId, data, { new: true });

        res.send(updatedHotel);
      }
    } catch (err) {
      res.status(404).send({ error: 'An error has occurred' });
    }
  });
};
