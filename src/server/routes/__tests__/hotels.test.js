const app = require('../../index');
const request = require('supertest');

describe('hotels', () => {
  describe('/GET hotels', () => {
    it('should GET all the hotels', (done) => {
      request(app)
        .get('/api/hotels?all=true')
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).not.toHaveLength(0);
          done();
        });
    });

    it('should GET hotels by query params', (done) => {
      request(app)
        .get('/api/hotels?stars[]=3&stars[]=4&stars[]=5&types[]=appartments&types[]=villa&sum=2313123123')
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).not.toHaveLength(0);
          done();
        });
    });
  });

  describe('/DELETE hotel', () => {
    it('should DELTE book from the hotel', (done) => {
      request(app)
        .delete('/api/hotels/5aef54990d3c6ffca8fea089')
        .set('Accept', 'application/json')
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body.book).toBeUndefined();
          done();
        });
    });
  });
});
